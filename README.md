# React Proyecto 5 - Clima
Obtención del clima de una ciudad y país

[Link to Web:](https://awesome-lalande-ddfc27.netlify.app/)

## ¿Herramientas utilizadas?
- Openweathermap
- useState
- Emotion styled
- Ajax Request/Promises


## ¿Cómo funciona?
- Mediante un formulario se obtienen los datos de la ciudad y el país para posteriormente enviarlos como POST en una consulta de Ajax, dicha consulta devuelve el clima de la ciudad si es encontrada.


## ¿Dudas?


